/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asm_java1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author DangHung
 */
public class employeeDao {
    public static Scanner scan = new Scanner(System.in);
    private List<Employee> list = new ArrayList<>();
    public void insertEmployee() {
        Scanner scan = new Scanner(System.in);
        Employee emp = new Employee();
//        Nhập và xuất danh sách nhân viên
        System.out.println("Nhập mã nhân viên: ");
        String empId = scan.nextLine();
        System.out.println("Nhập lương nhân viên: ");
        double salary = scan.nextInt();
        System.out.println("Nhập họ tên nhân viên: ");
        scan.nextLine();
        String fullName = scan.nextLine();
        emp.setEmpId(empId);
        emp.setSalary(salary);
        emp.setFullName(fullName);
        list.add(emp);
    }

    public void showEmployee() {
        Scanner scan = new Scanner(System.in);
        for (Employee emp : list) {
            System.out.println("Mã nhân viên: " + emp.getEmpId());
            System.out.println("Lương nhân viên: " + emp.getSalary());
            System.out.println("Họ và tên: " + emp.getFullName());
        }
//        Xuất danh sách nhân viên ra màn hình

    }

    public void findById() {
//        Tìm và hiển thị nhân viên theo mã nhập từ bàn phím
    }

    public void deleteById() {
//        Tìm và hiển thị nhân viên theo mã nhập từ bàn phím
    }

    public void updateById() {
//        Cập nhật thông tin nhân viên theo mã nhập từ bàn phím
    }

    public void findBySalary() {
//        Tìm các nhân viên theo khoảng lương nhập từ bàn phím
    }

    public void sortByName() {
//        Sắp xếp nhân viên theo họ và tên 
    }

    public void sortByIncome() {
//        Sắp xếp nhân viên theo thu nhập
    }

    public void topFiveEmployee() {
//        Xuất 5 nhân viên có thu nhập cao nhất
    }
}
