/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asm_java1;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author DangHung
 */
public class Employee {

    private String empId;
    private double salary;
    private String fullName;
    private double gpa;

    public Employee() {
    }

    public Employee(String empId, double salary, String fullName, double gpa) {
        this.empId = empId;
        this.salary = salary;
        this.fullName = fullName;
        this.gpa = gpa;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public double getGpa() {
        return gpa;
    }

    public void setGpa(double gpa) {
        this.gpa = gpa;
    }

}
