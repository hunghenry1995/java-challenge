/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package asm_java1;

import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author DangHung
 */
public class ASM_Java1 {

    static Scanner scan = new Scanner(System.in);
    static employeeDao empDao = new employeeDao();
    static tryCatch tc = new tryCatch();

    public static void main(String[] args) {

        menuCase();
    }

    public static void menuList() {

        System.out.println("1. Nhập và xuất danh sách nhân viên                       *");
        System.out.println("2. Xuất danh sách nhân viên ra màn hình                   *");
        System.out.println("3. Tìm và hiển thị nhân viên theo mã nhập từ bàn phím     *");
        System.out.println("4. Xóa nhân viên theo mã nhập từ bàn phím                 *");
        System.out.println("5. Cập nhật thông tin nhân viên theo mã nhập từ bàn phím  *");
        System.out.println("6. Tìm các nhân viên theo khoảng lương nhập từ bàn phím   *");
        System.out.println("7. Sắp xếp nhân viên theo họ và tên                       *");
        System.out.println("8. Sắp xếp nhân viên theo thu nhập                        *");
        System.out.println("9. Xuất 5 nhân viên có thu nhập cao nhất                  *");
     

    }

    public static void menuCase() {
        int x = 0;

        do {
            menuList();
            System.out.println("Nhập số từ 1 đến 10: ");

            try {
                int n = scan.nextInt();
                scan.nextLine();
                switch (n) {
                    case 1:
                        System.err.println("Bạn chọn nhập nhân viên");
                        empDao.insertEmployee();
                        menuCase();
                        break;
                    case 2:
                        System.err.println("Bạn chọn hiện thông tin nhân viên");
                        empDao.showEmployee();
                        break;
                    case 3:
                        System.err.println("*3. Tìm nhân viên theo mã");
                        empDao.findById();
                        break;
                    case 4:
                        System.err.println("*3. Xóa nhân viên theo mã    *");
                        empDao.deleteById();
                        break;
                    case 5:
                        System.err.println("Cập nhật thông tin nhân viên theo mã");
                        empDao.updateById();
                        break;
                    case 6:
                        System.err.println("Tìm các nhân viên theo khoảng lương");
                        empDao.findBySalary();
                        break;
                    case 7:
                        System.err.println("Sắp xếp nhân viên theo họ và tên");
                        empDao.sortByName();
                        break;
                    case 8:
                        System.err.println("Sắp xếp nhân viên theo thu nhập");
                        empDao.sortByIncome();
                        break;
                    case 9:
                        System.err.println("Xuất 5 nhân viên có thu nhập cao nhất");
                        empDao.topFiveEmployee();
                        break;
                    case 10:
                        System.err.println("hi");
                        break;
                    default:
                        System.err.println("Từ 1 đến 10 mà ");
                }
            } catch (InputMismatchException ix) {
                System.err.println("Nhập sai r bạn oiii");

                scan.nextLine();
                menuCase();
                return;
            }
            x = 1;
        } while (x != 1);

    }

    public static boolean chooseMenu(int n) {
        do {
            menuList();
        } while (n <= 10);
        return false;
    }
}
